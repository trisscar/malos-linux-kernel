VERSION=4.19.1; export VERSION
cp /boot/config-$(uname -r) .config
make -k -j 8 menuconfig
make -k -j 8 # do not remove this line, it is necessary.
make -k -j 8 modules
make -k -j 8 install
make -k -j 8 modules_install
update-initramfs -c -k $VERSION
update-grub
